Vue.component("peer-video",{
    props:["initcode","srcobj"],
    template:`
    <div>
        <video width="600" height="400" style="background-color: gray;" v-bind:src-object.prop.camel = "srcobj" autoplay></video>
        <div class="jumbotron" id="signal-text">{{initcode}}</div>
    </div>
    `
});

Vue.component("code-form",{
    data:function(){
        return{
            incomingCode:""
        }
    },
    template:`
    <div class="form-group">
        <textarea v-model="incomingCode" class="form-control mb-2" id="initiator-code-div" rows="3" placeholder="Paste the code from the other browser here..."></textarea>
        <button type="submit" class="btn btn-primary" v-on:click="onSubmitBtnClick">Submit</button>
    </div>
    `,

    methods:{
        onSubmitBtnClick:function(){
            this.$emit("incomingcode",this.incomingCode);
            this.incomingCode = "";
        }
    }
})


let app = new Vue({
    el:"#app",
    data:{
        initCode:"",
        streamSrc: null,
        peer:null
    },

    mounted:function(){
        navigator.mediaDevices.getUserMedia({
            video:true,
            audio:true
        }).then(this.setupPeer).catch(err => console.log(err));
    },

    methods:{
        setupPeer: function(stream){
            this.peer = new SimplePeer({
                initiator: location.hash === "#1",
                trickle: false,
                stream: stream
            });

            this.peer.on('signal', data => {
                this.initCode = JSON.stringify(data);
                
                let range = document.createRange();
                range.selectNode(document.getElementById("signal-text"));
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(range);
            });

            this.peer.on('error', err => console.error(err));

            this.peer.on('connect', () => {
                console.log("CONNECTED")
                this.initCode = "";
            });

            this.peer.on('stream',stream => {
                console.log("STREAM");
                this.streamSrc = stream;
            });

            this.peer.on('data',data => {
                console.log("New data: " + data);
            })

        },

        registerConnectionCode: function(incomingCode){
            this.peer.signal(incomingCode);
        }
    }
    
});